import React from 'react';
import "./assets/css/reset.css";
import styled from "styled-components";
import { Route, Switch } from 'react-router-dom';
import ViewContainer from './pages/view/ViewContainer';

const App = () => {
    return (
        <Wrap>
            <Switch>
                <Route path="/" component={ViewContainer}/>
             </Switch>
        </Wrap>
    );
};

const Wrap = styled.div`
    width: 1080px;
    margin: 150px auto 0;
    box-sizing: border-box;
    padding: 10px;

    
`;

export default App;
