import {action, observable} from "mobx";
import agent from '../agent';

class CommonStore {
    @observable externalCode = {
        position: "head"
    };
    @observable tagLists = [];
    @observable codeId = "";
    @observable modifiedCode = {};

    @action setResetCode() {
        this.externalCode.url = "";
        this.externalCode.type = "";
        this.externalCode.code = "";
        this.externalCode.position = "";
    }

    @action setExternalCodeType(type) {
        this.externalCode.type = type;
    }

    @action setExternalCodeUrl(url) {
        this.externalCode.url = url;
    }

    @action setExternalCode(code) {
        this.externalCode.code = code;
    }

    @action setExternalCodePosition(position) {
        this.externalCode.position = position;
    }

    @action setTagLists(list) {
        this.tagLists = list;
    }

    @action setCodeId(code) {
        this.codeId = code;
    }

    getAllCode() {
        return agent.Common.getAllCode();
    }

    createCode() {
        return agent.Common.createCode(this.externalCode);
    }

    deleteCode(codeId) {
        return agent.Common.deleteCode(codeId);
    }

    modifiedCode(codeId) {
        return agent.Common.modifiedCode(codeId, this.modifiedCode);
    }
}

export default new CommonStore();
