import React from 'react';
import {inject, observer} from "mobx-react";
import {Route, Switch, withRouter} from "react-router-dom";
import ViewTemplate from './templates/ViewTemplate';

export default
@inject( 'CommonStore' )
@withRouter
@observer
class ViewContainer extends React.Component {
    render() {
        const { match } = this.props;

        return (
            <>
                <Switch>
                    <Route exact path={match.url} component={ViewTemplate} />
                </Switch>
            </>
        )
    }
}
