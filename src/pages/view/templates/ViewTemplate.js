import { inject, observer } from 'mobx-react';
import React, { useEffect, useState } from 'react';
import { useHistory } from 'react-router-dom';
import styled from 'styled-components';

const ViewTemplate = inject("CommonStore")(observer((props) => {
    useEffect(() => {
        props.CommonStore.getAllCode()
            .then(data => {
                props.CommonStore.setTagLists(data.external_code);
            });
    }, []);

    const [isChange, setIsChange] = useState(false);

    const onChange = (e) => {
        let name = e.target.name;
        
        if(name === "type") {
            props.CommonStore.setExternalCodeType(e.target.value);
        } else if (name === "url"){
            props.CommonStore.setExternalCodeUrl(e.target.value);
        } else if (name === "position"){
            props.CommonStore.setExternalCodePosition(e.target.value);
        } else if (name === "code") {
            props.CommonStore.setExternalCode(e.target.value);
        }
    };

    const onRemove = (id) => {
        props.CommonStore.deleteCode(id);
        alert("삭제 되었습니다");
        console.log(props.CommonStore.tagLists);
    }

    const onsubmit = () => {
        if (props.CommonStore.externalCode.type && props.CommonStore.externalCode.url && props.CommonStore.externalCode.code && props.CommonStore.externalCode.position) {
            props.CommonStore.createCode();
            alert("생성되었습니다.");
            props.CommonStore.setResetCode();
        } else {
            alert("내용을 입력해주세요");
        }
    };

    return(
        <Wrap>
            <div className="container">
                <p className="title">생성 하고 싶은 tag</p>

                <div className="create">
                    <div>
                        <p>type</p>
                        <input 
                            name="type" 
                            placeholder="ex) GTM" 
                            onChange={onChange} 
                            value={props.CommonStore.externalCode.type || ''}
                        />
                    </div>
                    <div>
                        <p>url</p>
                        <input 
                            name="url" 
                            placeholder="ex) test.co.kr" 
                            onChange={onChange} 
                            value={props.CommonStore.externalCode.url || ''}
                        />
                    </div>
                    <div className="position">
                        <p>position</p>
                        <select name="position" onChange={onChange} value={props.CommonStore.externalCode.position}>
                            <option value="head">head</option>
                            <option value="body">body</option>
                        </select>
                    </div>
                    <div className="code">
                        <p>code</p>
                        <textarea 
                            name="code" 
                            placeholder="ex) <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src='https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);})(window,document,'script','dataLayer','GTM-NFJ7XRM');</script>" 
                            onChange={onChange} 
                            value={props.CommonStore.externalCode.code}
                        />
                    </div>
                </div>

                <div className="bt_box">
                    <button onClick={onsubmit}>생성</button>
                </div>
            </div>

            <div className="list_box">
                <p className="title">tag 목록</p>
                <div className="container guide">
                    <div>type</div>
                    <div>url</div>
                    <div>position</div>
                    <div>code</div>
                </div>
                {
                    props.CommonStore.tagLists && props.CommonStore.tagLists.map((list) => {
                        return (
                            <div key={list.id} className="container list">
                                <div>{list.type}</div>
                                <div>{list.url}</div>
                                <div>{list.position}</div>
                                <div>{list.code}</div>
                                <div className="bts_box">
                                    {
                                        isChange ? (
                                            <>
                                                <button 
                                                    className="modified done" 
                                                    onClick={()=>{
                                                        props.CommonStore.modifiedCode(`${list.id}`)
                                                        setIsChange(false)
                                                    }}
                                                    // onClick={()=>setIsChange(false)}
                                                >수정완료</button>
                                            </>
                                        ) : (
                                            <>
                                                <button 
                                                    className="delete"
                                                    onClick={()=>onRemove(list.id)}
                                                >삭제</button>
                                            </>
                                        )
                                    }
                                    
                                </div>
                            </div>
                        )
                    })
                }
            </div>
        </Wrap>
    );
}));

const Wrap = styled.div`
    margin-top: 20px;

    .title {
        margin: 40px 0;
        font-size: 30px;
        text-align: center;
    }

    > .container {
        border: 1px solid gray;
        padding: 0 10px 20px;

        .create{
            display: flex;
            justify-content: space-between;

            p {
                padding: 10px 0;
            }

            div {
                &.position {
                    width: 70px;
                }
                &.code {
                    width: 300px;
                    textarea {
                        box-sizing: border-box;
                        width: 100%;
                        height: 200px;
                    }
                }
            }
        }
        
        .bt_box {
            margin-top: 50px;
            text-align: center;
            button {
                border: 1px solid black;
                padding: 5px 15px;
                border-radius: 20px;
            }
        }
        
    }

    .list_box{ 
        margin-top: 40px;

        border: 1px solid gray;

        > .container {
            display: flex;
            justify-content: space-between;
            align-items: center;
            > div {
                padding: 0 10px;
                border-radius: 10px;
                width: 25%;
                overflow-wrap: break-word;
                line-height: 20px;
            }

            &.list {
                position: relative;
                padding: 10px 0;

                & + div {
                    border-top: 1px solid gray;
                }

                .bts_box {
                    width:100%;
                    position: absolute;
                    left: 100%;
                }

                button {
                    width: 40px;
                    border: 1px solid gray;
                    border-radius: 10px;
                    & + button {
                        margin-left: 10px;
                    }

                    &.modified {
                        background-color: #000;
                        color: #fff;
                        &.done {
                            color: #000;
                            background-color: #fff;
                            width: 80px;
                        }
                    }

                    &.delete {
                        background-color: red;
                        color: #fff;
                    }
                }
            }

            &.guide {
                margin-bottom: 20px;
                > div {
                    text-align: left;
                    font-size: 24px;
                }
            }
        }
    }
`;

export default ViewTemplate;