import React from 'react';
import {inject, observer} from "mobx-react";
import {Route, Switch, withRouter} from "react-router-dom";
import {AuthTemplate} from './templates';

export default
@inject( 'AuthStore' )
@withRouter
@observer
class AuthContainer extends React.Component {
    render() {
        const { match } = this.props;

        return (
            <>
                <Switch>
                    <Route exact path={match.url} component={AuthTemplate} />
                </Switch>
            </>
        )
    }
}
