const {
    addDecoratorsLegacy,
    disableEsLint,
    override,
} = require("customize-cra");

if (process.env.NODE_ENV === 'development') {
    process.env.BABEL_ENV = 'development';
    process.env.REACT_APP_ROOT = 'https://okr.luvd.co.kr';
    process.env.REACT_APP_FRONT_ROOT = 'http://dev.fordate.org:3000';
} else {
    process.env.BABEL_ENV = 'production';
    process.env.REACT_APP_ROOT = 'https://okr.luvd.co.kr';
    process.env.REACT_APP_FRONT_ROOT = 'https://form.fordate.org';
}

module.exports = {
    webpack: override(disableEsLint(), addDecoratorsLegacy()),
};
